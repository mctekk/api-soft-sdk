# Baka\Soft\Api

Baka Light SDK , instead of having to create a new sdk for every single API we build 
with this trait just create your API controller, and call you api like normal ;)

@todo change name to Baka\SDK\

## Configure

**Router**

```
<?php
$router->addGet('/api/v{version}/:params', 'Api::transporter');
$router->addGet('/api/v{version}/([^\/]+)', 'Api::transporter');
$router->addGet('/api/v{version}/([^\/]+)/([^\/]+)', 'Api::transporter');
$router->addGet('/api/v{version}/([^\/]+)/([^\/]+)/([^\/]+)', 'Api::transporter');
$router->addGet('/api/v{version}/([^\/]+)/([^\/]+)/([^\/]+)/([^\/]+)', 'Api::transporter');
```

## Controller


```
<?php

namespace Phalcon\Controllers;

class ApiController extends ControllerBase
{

    use \Baka\Soft\Api\BehaviorTrait;

    /**
     * setup the private keys
     *
     * @return void
     */
    public function onConstruct()
    {
        $this->publicKey = '';
        $this->privateKey = '';
    }

}

});
```

